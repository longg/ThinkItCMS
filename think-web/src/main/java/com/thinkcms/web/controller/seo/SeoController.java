package com.thinkcms.web.controller.seo;

import com.thinkcms.service.api.seo.SeoService;
import com.thinkcms.core.annotation.Logs;
import com.thinkcms.core.enumerate.LogModule;
import com.thinkcms.core.utils.ApiResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 内容 前端控制器
 * </p>
 *
 * @author LG
 * @since 2019-10-30
 */
@Validated
@RestController
@RequestMapping("analysis")
public class SeoController {

    @Autowired
    SeoService seoService;

    @Logs(module = LogModule.CONTENT, operation = "分析文章 详情")
    @GetMapping("index")
    public ApiResult analysis(@NotBlank @RequestParam String id) {
        return seoService.analysisContent(id);
    }


}
