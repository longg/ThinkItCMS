package com.thinkcms.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan(basePackages ={"com.thinkcms.*.mapper"})
@ComponentScan(basePackages = {
        "com.thinkcms.core.*",
        "com.thinkcms.security.*",
        "com.thinkcms.web.*",
        "com.thinkcms.system.*",
        "com.thinkcms.service.*",
        "com.thinkcms.freemark.*",
        "com.thinkcms.addons.*",
        "com.thinkcms.seo.*",
        })
public class ThinkItCMSApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThinkItCMSApplication.class, args);
    }

}
