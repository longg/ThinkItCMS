package com.thinkcms.freemark.tools;
import com.thinkcms.service.dto.content.ContentDto;
import com.thinkcms.core.utils.Checker;

import java.util.HashMap;

public class ContentHelp extends HashMap<String, Object> {

    public ContentHelp(ContentDto contentDto){
        this.buildContentHelp(contentDto);
    }


    public void buildContentHelp(ContentDto contentDto){
        if(Checker.BeNotNull(contentDto)){
            this.put("contentId",contentDto.getId());
            this.put("categoryId",contentDto.getCategoryId());
            this.put("hasRelated",contentDto.getHasRelated());
            this.put("hasTag",Checker.BeNotBlank(contentDto.getTagIds()));
            this.put("title", Checker.BeNotBlank(contentDto.getTitle()) ? contentDto.getTitle() : "");
            this.put("keywords", Checker.BeNotBlank(contentDto.getKeywords()) ? contentDto.getKeywords() : "");
            this.put("description", Checker.BeNotBlank(contentDto.getDescription()) ? contentDto.getDescription() : "");
        }
    }

}
