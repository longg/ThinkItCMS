package com.thinkcms.service.api.seo;

import com.thinkcms.core.utils.ApiResult;

import java.util.List;

/**
 * <p>
 * 内容 服务类
 * </p>
 *
 * @author LG
 * @since 2019-10-30
 */
public interface SeoService  {

    /**
     * 关键字提取
     * @param contentId
     * @return
     */
    List<String> extractKeywordByContentId(String contentId);


    /**
     * 关键字提取
     * @param contentId
     * @return
     */
    List<String> extractKeywordByContent(String content);


    /**
     * 获取 content
     * @param path
     * @return
     */
    ApiResult getFileContentByPath(String path);

    /**
     * 分析文章详情
     * @param id
     * @return
     */
    ApiResult analysisContent(String id);
}