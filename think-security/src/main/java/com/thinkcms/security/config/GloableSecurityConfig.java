package com.thinkcms.security.config;

import com.thinkcms.security.custom.CustomUserAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class GloableSecurityConfig extends WebSecurityConfigurerAdapter {


	@Autowired
	CustomUserAuthenticationProvider customUserAuthenticationProvider;

    @Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/error/**","/favicon.ico","/socket/**","/stomp/**","/api/**","/sqlMonitor/**","/plugins/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.requestMatchers().anyRequest()
		.and()
		.authorizeRequests()
		.antMatchers("/oauth/*","/user/info","/minapp/**","/error","/content/searchKeyWord").permitAll();
	}


	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		//return super.authenticationManagerBean();
		return authenticationManager();
	}

	/**
	 * Spring Security认证服务中的相关实现重新定义
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// 加入自定义的安全认证
		auth.authenticationProvider(customUserAuthenticationProvider);
	}

}
