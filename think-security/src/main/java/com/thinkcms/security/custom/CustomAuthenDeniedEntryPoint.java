package com.thinkcms.security.custom;

import com.thinkcms.core.utils.ApiResult;
import com.thinkcms.core.utils.WebUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * @Author LG
 * @Description //token 认证失败统一处理即验证jwt是否合法
 * @Date 10:33 2019/4/30
 * @Param
 * @return
 **/
@Slf4j
@Component
public class CustomAuthenDeniedEntryPoint implements AuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
		log.error("-----------登录失效--------------");
		//response.setStatus(HttpStatus.UNAUTHORIZED.value());
		WebUtil.write(response, ApiResult.result(HttpStatus.UNAUTHORIZED.value(),e.getMessage()));
	}
}
