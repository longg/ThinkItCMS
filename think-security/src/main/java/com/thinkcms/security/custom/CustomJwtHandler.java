package com.thinkcms.security.custom;

import com.thinkcms.core.enumerate.UserForm;
import com.thinkcms.core.handler.CustomException;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class CustomJwtHandler extends AbsCustomJwtHandler {
    @Override
    public void handlerJwtToken(Authentication authentication ,JwtTokenStore tokenStore ) throws CustomException {
        //读取详细信息-commonJwtAccessConvert extractAuthentication方法
        String tokenValue = authentication.getPrincipal().toString();//读取token信息
        OAuth2Authentication oauth=tokenStore.readAuthentication(tokenValue);
        Map<String, Object> details = (Map<String, Object>)oauth.getDetails();
        boolean isAppUser=details.containsKey(UserForm.USER_FORM.getCode())&& UserForm.APP_USER.getCode().equals(details.get(UserForm.USER_FORM.getCode()));
        if(isAppUser){
            super.handAppUserSession(details,tokenValue);
        }else{
            super.handPlatUserSession(details, tokenValue);
        }
    }
}
