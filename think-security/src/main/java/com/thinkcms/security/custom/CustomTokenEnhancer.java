package com.thinkcms.security.custom;

import com.thinkcms.core.utils.SecurityConstants;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: CustomTokenEnhancer
 * @Author: LG 令牌增强器可以添加自己的json字段返回
 * @Date: 2019/4/26 16:23
 * @Version: 1.0
 **/
public class CustomTokenEnhancer implements TokenEnhancer {


    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {
        CustomJwtUser user = ((CustomJwtUser)oAuth2Authentication.getUserAuthentication().getPrincipal());
        final Map<String, Object> additionalInfo = new HashMap<>();
        additionalInfo.put(SecurityConstants.USER_ID, user.getUserId());
        additionalInfo.put(SecurityConstants.DEPT_ID, user.getDeptId());
        additionalInfo.put(SecurityConstants.USER_NAME,user.getUserName());
        additionalInfo.put(SecurityConstants.NAME,user.getName());
        additionalInfo.put(SecurityConstants.ROLE_SIGNS,user.getRoleSigns());
        ((DefaultOAuth2AccessToken) oAuth2AccessToken).setAdditionalInformation(additionalInfo);
        return oAuth2AccessToken;
    }
}
