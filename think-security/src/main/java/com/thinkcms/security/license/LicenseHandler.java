package com.thinkcms.security.license;
import com.thinkcms.core.handler.CustomException;
import com.thinkcms.core.utils.ApiResult;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;
@Slf4j
public class LicenseHandler implements InvocationHandler {

    private Object target;

    private Class cls;

    private Interceptor interceptor;

    public LicenseHandler(){

    }

    public LicenseHandler( Object target,Class cls) throws Exception {
        this.target=target;
        this.cls=cls;
        this.interceptor=(Interceptor)cls.newInstance();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Map<String,Object> propertie=interceptor.before(proxy,target,method,args);
        if(propertie.isEmpty()){
           log.error("20031:license证书不存在");
           throw new CustomException(ApiResult.result(20031));
        }
        Object object= method.invoke(target,args[0],propertie);
        interceptor.after(proxy,target,method,args,propertie);
        return object;
    }
}
