package com.thinkcms.security.license;

import java.util.Map;

public interface VerifyLicense {

     Boolean verify(String licensePath, Map<String,Object> propertie) throws Exception;

}
