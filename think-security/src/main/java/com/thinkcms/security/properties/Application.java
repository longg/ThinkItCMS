package com.thinkcms.security.properties;

import lombok.Data;

import java.util.List;

@Data
public class Application {
    public String appName;
    public List<String> ignoreApi;
}
