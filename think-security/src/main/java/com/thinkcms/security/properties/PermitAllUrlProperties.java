package com.thinkcms.security.properties;

import com.thinkcms.core.utils.Checker;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "permission")
public class PermitAllUrlProperties {

    public List<Application> application=new ArrayList<>();

    public boolean ckTheSameApi(String name,String apiUrl){
        if(Checker.BeBlank(name)){
            return false;
        }
        boolean res=false;
        look:
        for (Application app : this.getApplication()) {
            if (name.equals(app.getAppName())) {
                for (String api : app.getIgnoreApi()) {
                    if (apiUrl.equals(api)) {
                        res = true;
                        break look;
                    }
                }
            }
        }
        return res;
    }
}
