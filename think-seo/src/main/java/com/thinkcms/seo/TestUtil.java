package com.thinkcms.seo;

import com.thinkcms.core.constants.Constants;
import com.thinkcms.core.utils.Checker;
import com.thinkcms.seo.seo.core.word.WordUtil;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class TestUtil {


    @Test
    public void testHtml() throws IOException {

        String path="E:\\portp\\webfile\\contactUs\\2019-12-09\\387240193268121600.html ";
        File file =new File(path);
        String html = FileUtils.readFileToString(file, Constants.DEFAULT_CHARSET_NAME);
        Document doc = Jsoup.parse(html);
//        String e =doc.select("meta[name=keywords]").get(0).attr("content");
//        System.out.println(e);
        Elements element=doc.getElementsByTag("script");
        for(Element e :element){
            System.out.println(Checker.BeNotBlank(e.html()));
        }
//        System.out.println(element.size());
//        System.out.println(element.html());
//        System.out.println(element.hasText());
//          Elements element =doc.select("meta[name=keywords]");
//         System.out.println(element.size());
//         System.out.println(element.get(0).attr("content"));
//         System.out.println(element.hasText());
    }


    @Test
    public void  extractSummary(){
        String cont="当时，我的第一感觉是，老师讲课的速度很快，只要有学生说懂了，她就马上进入下个环节。这是一个面临着中考的班级，快速讲课是应该的，但这需要学生有坚实的基础知识和理解能力，才能跟上讲课的速度。而坚实的基础知识和理解能力，来自学生日常主动预习功课和锲而不舍的钻研精神，即我们常说的状元精神。\n" +
                "\n" +
                "接下来的自习时间里，我看到两个女孩在座位上聊天嬉笑，根本无意学习。我走过去，笑着对她们说：“小同学，我真羡慕你们今天能有这么好的学习条件。我像你们这般年纪时，学习条件很糟糕，学校上课不正常，教材简单枯燥，参考书籍匮乏，求学非常艰难。你们现在什么都不缺，唯缺珍惜读书时光的理智。这读书的黄金时光转瞬即逝，你们要珍惜呀。”说着说着，我的眼泪又在眼眶里打转了。\n" +
                "\n" +
                "两位学生盯着我，其中一位说：“阿姨，我懂学习的重要，可是真的好难，有时明明在课堂上听懂了，回去做作业又忘了。我好笨，越学越没信心。”\n" +
                "\n" +
                "“孩子，潜意识的自信是非常重要的，你要相信自己的智力，别太低估了自己。不过，学习方法还是要讲究的。你要养成预习的习惯，把第二天老师要讲的内容多看几遍，不懂的画在书上或记在笔记本上，听课时，针对性的听老师讲这方面的内容。还不懂，可以在课堂上问老师，或课后问都行，似懂非懂的题目一定要弄懂，老师是欢迎学生主动提问的。你只要树立信心，勤学好问，努力不懈地求学，会把难字攻克的。试试看!”我信任地竖起大拇指挺她。\n" +
                "\n" +
                "“谢谢阿姨，我懂了。”\n" +
                "\n" +
                "后来，这两个女孩拿着试卷到教研室找老师去了。但愿我的话能帮助她们拾回学习的信心，快乐读书，天天进步。\n" +
                "\n" +
                "平时，我们该如何安排好自己的学习呢?\n" +
                "\n" +
                "首先，要明确学习的意义。一个人，只有通过学习，才能获取文化修养和知识力量，提升自己生存的幸福指数。因此，学习应该是主动的、快乐的人生体验，而不是被动的、勉为其难的苦差事。明白了这些，根据实际情况制订一个适合自己的学习方法，主要注意以下两方面：\n" +
                "\n" +
                "(1).知道自己擅长学科和薄弱学科。学习过程中，可以扬长，但不能避短，而且还应该在短上下功夫，提高一点算一点。因为你还是专职学生，要参加中考、高考，乃至大学毕业考。弱项学科成绩太差，会拖累考试总分，很惨的。\n" +
                "\n" +
                "(2).上课专心听，认真做笔记。课后做好各门功课解题的归纳总结，学会举一反三，不懂就虚心向老师或同学请教。虽然嫉妒是人与生俱来的本能，但求学者的真诚和虚心，还是能化解这个人性弱点的。处理得当，可以产生良性的竞争，对大家的学习都有促进作用。";
       String su= WordUtil.extractSummary(cont,100);
        System.out.println(su);
    }

}
