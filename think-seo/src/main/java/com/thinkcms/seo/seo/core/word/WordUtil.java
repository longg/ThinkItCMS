package com.thinkcms.seo.seo.core.word;
import com.hankcs.hanlp.HanLP;
import com.thinkcms.core.utils.Checker;

import java.util.ArrayList;
import java.util.List;

public class WordUtil {

    public static List<String> extractKeyword(String content,int count){
        List<String> keywordList = new ArrayList<>(16);
        if(Checker.BeNotBlank(content)){
            content = HtmlUtil.getTextFromHtml(content);
            keywordList = HanLP.extractKeyword(content, count==0?1:count);
        }
        return keywordList;
    }


    public static String extractSummary(String content,int count){
        String summary ="";
        if(Checker.BeNotBlank(content)){
            content = HtmlUtil.getTextFromHtml(content);
            summary = HanLP.getSummary(content, count==0?150:count);
        }
        return summary;
    }



}
