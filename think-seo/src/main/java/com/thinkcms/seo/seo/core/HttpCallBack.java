package com.thinkcms.seo.seo.core;

import com.thinkcms.core.utils.ApiResult;
import okhttp3.Call;

public interface HttpCallBack {
    // https://blog.csdn.net/weixin_38796720/article/details/88991153

     void onSuccess(Call call, ApiResult apiResult);


     void onFailure(Call call, Exception e);

}
