package com.thinkcms.seo.seo.core.word;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HtmlUtil {

    private static final String regEx_script = "<script[^>]*?>[\\s\\S]*?<\\/script>"; // 定义script的正则表达式
    private static final String regEx_style = "<style[^>]*?>[\\s\\S]*?<\\/style>"; // 定义style的正则表达式
    private static final String regEx_html = "<[^>]+>"; // 定义HTML标签的正则表达式
    private static final String regEx_space = "\\s*|\t|\r|\n";//定义空格回车换行符
    /**
     * @param htmlStr
     * @return
     * 删除Html标签
     */
    public static String delHTMLTag(String htmlStr) {
        Pattern p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
        Matcher m_script = p_script.matcher(htmlStr);
        htmlStr = m_script.replaceAll(""); // 过滤script标签
        Pattern p_style = Pattern.compile(regEx_style, Pattern.CASE_INSENSITIVE);
        Matcher m_style = p_style.matcher(htmlStr);
        htmlStr = m_style.replaceAll(""); // 过滤style标签
        Pattern p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
        Matcher m_html = p_html.matcher(htmlStr);
        htmlStr = m_html.replaceAll(""); // 过滤html标签
        Pattern p_space = Pattern.compile(regEx_space, Pattern.CASE_INSENSITIVE);
        Matcher m_space = p_space.matcher(htmlStr);
        htmlStr = m_space.replaceAll(""); // 过滤空格回车标签
        htmlStr = htmlStr.replace("&rdquo","").replace("&ldquo","")
        .replace("&mdash","");
        return htmlStr.trim(); // 返回文本字符串
    }
    public static String getTextFromHtml(String htmlStr){
        htmlStr = delHTMLTag(htmlStr);
        htmlStr = htmlStr.replaceAll(" ", "");
        //htmlStr = htmlStr.replace("。","");
        return htmlStr;
    }
    public static void main(String[] args) {
        String str = "<div id=\"wenzhangziti\" class=\"article 393503\"><p>　　<a href=\"http://www.duwenzhang.com/huati/pengyou/index1.html\">朋友</a>一个暖心的字眼，是一生<a href=\"http://www.duwenzhang.com/huati/yongyou/index1.html\">拥有</a>最大的财富，在你落魄的时候，闻之而来的亲人，不离不弃，拉你一把，送你入正途的人。他不会嫌弃你的现在，哪怕你落魄不堪。他只是记得当初彼此真心的交往，彼此<a href=\"http://www.duwenzhang.com/huati/kuaile/index1.html\">快乐</a>拥有，没有世俗的眼光，没有利义取舍的心。</p>\n" +
                "<p>　　 <a href=\"http://www.duwenzhang.com/huati/tongnian/index1.html\">童年</a>的时光是快乐的，是童真的，叫人年长后回味一生的光阴。那时候不知道世间的冷暖，没有地位的高低之分，只有同伴间的嬉笑打闹。我有三个一天玩到晚，三个年长于我的同伴。我记得我是个胆小而又内向的<a href=\"http://www.duwenzhang.com/huati/haizi/index1.html\">孩子</a>，但是我们在一起，有说不完的话，有数不尽的嬉笑打闹。经常背着<a href=\"http://www.duwenzhang.com/huati/fumu/index1.html\">父母</a>，去游游泳去野地里游玩，偷偷打邻居家的老母鸡，大黄狗。没事的时候扑克牌能够玩一整天，脸上贴满了纸条，或是一杯杯的水，被输者喝下度，喝的肚子疼……</p>\n" +
                "<p>　　 童年的光阴是<a href=\"http://www.duwenzhang.com/huati/meihao/index1.html\">美好</a>的，但是却转瞬即逝。一转眼我的那几个玩伴，有两个去了城里上学，另一个参加了工作，我也被我父母送到了城里读书。从此我们彼此很少见面，只是到了年底，或是暑假，才能彼此见面。见面时没有了彼此的打闹，没有了那么多的话语，只是说说彼此所处的境遇，说说外面各自的见闻，然后就是一起看看电视而已……</p>\n" +
                "<p>　　 也许是我天生的性情，也许是我太过于父母的溺爱，我非常不适应外面寄宿学校的<a href=\"http://www.duwenzhang.com/wenzhang/shenghuosuibi/\">生活</a>。从此我的<a href=\"http://www.duwenzhang.com/huati/shengming/index1.html\">生命</a>似乎没有了快乐得二字。每天晚上难于入眠，有些许的噪音而不能入睡，白天还要一天的学习。那个时候每天都无时无刻不<a href=\"http://www.duwenzhang.com/huati/xiangnian/index1.html\">想念</a>父母，想念自己的那个小山村。身体日渐消瘦，久而久之，我撑不起自己瘦弱的身体，终于有一天我选择了辍学。高中没有毕业，终于圆了我朝思的<a href=\"http://www.duwenzhang.com/huati/mengxiang/index1.html\">梦想</a>，回到了小山村，回到了父母身边。</p>\n" +
                "<p>　　 自此我寡言少语，入睡困难的病痛，日益加深。随之而来的是头晕头痛，精神萎靡不振，饭量急剧减少，胸闷胸痛，随之而来。那个时候我感到了，邻里街坊，别人异样的眼光，偶尔间对我的闪烁其辞。我也时常听到和感触到父母对我的担忧。那个时候我感触不到阳光的灿烂与<a href=\"http://www.duwenzhang.com/huati/meili/index1.html\">美丽</a>，感触不到生活的<a href=\"http://www.duwenzhang.com/huati/mubiao/index1.html\">目标</a>与前途。突然有一天，脑海中突然间蹦出死亡的念头，用死去结束病痛与<a href=\"http://www.duwenzhang.com/huati/gudu/index1.html\">孤独</a>。</p>\n" +
                "<p>　　 </p>\n" +
                "<p>　　我偷偷溜出家门，想一饮而下瓶中的农药，想结束此时所有的<a href=\"http://www.duwenzhang.com/huati/tongku/index1.html\">痛苦</a>。在反复的思想斗争中，理智与清醒占据了上风，药瓶被我摔了个粉碎。我想起我是个独生子，知道了父母的存在，想到了日后的孤苦无依，<a href=\"http://www.duwenzhang.com/huati/yanlei/index1.html\">眼泪</a>瞬间夺框而出……偷偷的我回了家，好像什么都没有发生。依旧我的，大门不出，依旧我的少言寡语，依旧我的闭门自封……</p>\n" +
                "<p>　　 记得 那是一个午后，我刚睡醒，在床前呆坐，突然间的敲门声，惊醒了我。没想到门外是我好久不见的哥们，没有朋友间久不重逢的<a href=\"http://www.duwenzhang.com/huati/xiyue/index1.html\">喜悦</a>与拥抱。我是一个似乎与世隔绝人，记得那个午后我们没有几句的话语，是我哥们偶尔的询问。我没有谈及我的病痛，但我偶尔<a href=\"http://www.duwenzhang.com/huati/ganjue/index1.html\">感觉</a>到，他对我如今的了解。 </p>\n" +
                "<p>　　从那以后，我的朋友没事的时候总来，和我谈心说话来。记得有一天午后，我哥们骑着摩托而来，在他的推搡下，我和他去游玩。从那以后和他去村里的熟人去闲坐，去玩棋，去打牌。渐渐的我多了些话语，多了些<a href=\"http://www.duwenzhang.com/huati/kaixin/index1.html\">开心</a>与<a href=\"http://www.duwenzhang.com/huati/weixiao/index1.html\">微笑</a>。有一天我朋友突然间和我说了今后的打算，叫我去学一门手艺。后来我的那两个哥们也回来了，他们都上了<a href=\"http://www.duwenzhang.com/wenzhang/xiaoyuanwenzhang/daxueshenghuo/\">大学</a>，在我感觉是见过大世面的人。那一下午，都在为我今后的出路而谋划，最终我选择了我如今的职业，选择了修理，选择了与父母的朝夕相伴。</p>\n" +
                "<p>　　 <a href=\"http://www.duwenzhang.com/wenzhang/renshengzheli/\">人生</a>的选择没有那么一帆风顺，记的当初，我初学而来，是门厅的冷落，甚至是熟人的冷嘲热讽。后来<a href=\"http://www.duwenzhang.com/huati/laopo/index1.html\">妻子</a>的到来，<a href=\"http://www.duwenzhang.com/huati/jiating/index1.html\">家庭</a>的组合，柴米油盐的琐碎，都需要<a href=\"http://www.duwenzhang.com/huati/jinqian/index1.html\">金钱</a>的支撑，叫我心灰意冷，有了<a href=\"http://www.duwenzhang.com/huati/fangqi/index1.html\">放弃</a>的念头。可是我又能去做什么，身体的孱弱，叫我何去何从。心里的苦水，和我的朋友说了不止多少次，可是他们都劝我坚持，劝我用心去想，有什么办法去开拓客源。人啊有时候就是这样，可能些许的话语，暖心的安慰与鼓励。就能够叫你冲出迷雾，迎来柳暗花明的那一天。 </p>\n" +
                "<p>　　 记得那时候每次的手头的紧缺，都是向我的哥们去借钱。每次都是<a href=\"http://www.duwenzhang.com/huati/xiangxin/index1.html\">相信</a>的目光，都是那句话，盛明会好的，日子一天天的会好的，有你想要的<a href=\"http://www.duwenzhang.com/huati/xingfu/index1.html\">幸福</a>…… </p>\n" +
                "<p>　　 日子在我和妻子的<a href=\"http://www.duwenzhang.com/huati/nuli/index1.html\">努力</a>下，在哥们关照鼓舞下，渐渐走上了正途。除了如今的修理，还有了自己的商店。其中有太多哥们金钱的援助，道路上的指引与参考。心中<a href=\"http://www.duwenzhang.com/huati/gandong/index1.html\">感动</a>话语我从没有说出过口，说给我的哥们听。因为我感觉到没有的必要。我不想用世间的词语，我怕一不小心玷污了彼此的那份情怀。只想用自己一生，去<a href=\"http://www.duwenzhang.com/huati/wennuan/index1.html\">温暖</a>，去<a href=\"http://www.duwenzhang.com/huati/huiyi/index1.html\">回忆</a>，去感知，去铭记此生的真正拥有……　　 作者屈盛明</p>\n" +
                "</div>";
         System.out.println(getTextFromHtml(str));
        //System.out.println(str.replaceAll("[^\u4E00-\u9FA5]", "") );
    }
}
