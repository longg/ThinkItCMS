package com.thinkcms.core.constants;

import lombok.Getter;

public enum DirectiveNameEnum {

	CMS_CONTENT_DIRECTIVE("_d_content","content","根据文章id 获取详情"),

	CMS_ONE_CONTENT_DIRECTIVE("_d_one_content","content","根据ID栏目获取第一条内容指令"),

	CMS_CATEGORY_LIST_DIRECTIVE("_d_content_list","contents","根据栏目获取栏目下的文章指令,用于生成栏目列表页"),

	CMS_CATEGORY_DIRECTIVE("_d_category","categorys","获取所有显示的栏目指令如首页的栏目展示"),

	CMS_CATEGORY_INFO_DIRECTIVE("_d_category_info","category","获取栏目详细信息"),

	CMS_CATEGORY_BY_CODE_DIRECTIVE("_d_category_by_code","categorys","根据栏目code 获取栏目详情指令"),

	CMS_CHILD_CATEGORY_DIRECTIVE("_d_child_category","childCategory","根据栏目id获取子栏目指令"),

	CMS_CATEGORY_CONTENT_FOR_PAGE_HOME_DIRECTIVE("_d_content_by_category","contentOfcategory","根据栏目code 获取指令栏目下的内容指令"),

	CMS_ATTACH_DIRECTIVE("_d_attach","attachs","当前内容下的附件"),

	CMS_CONTENT_TAGS_DIRECTIVE("_d_content_tags","tags","当前内容下的标签"),

	CMS_TAGS_DIRECTIVE("_d_tags","tags","当前内容下的标签"),

	CMS_SITE_DIRECTIVE("_d_site","site","获取网站配置"),

	CMS_CONTENT_RELATED_DIRECTIVE("_d_related","relateds","获内容推荐"),

	CMS_CONTENT_NEXT_DIRECTIVE("_d_content_next","next","获取下一篇文章"),

	CMS_CONTENT_PREVIOUS_DIRECTIVE("_d_content_pre","previous","获取下一篇文章"),

	CMS_CLICKS_TOP_DIRECTIVE("_d_clicks_top","tops","热门点击"),

	CMS_RECOMM_DIRECTIVE("_d_recomms","recomms","获取推荐内容"),

	CMS_HOT_DIRECTIVE("_d_hots","hots","获取热门内容"),

	CMS_NOTICE_DIRECTIVE("_d_notices","notices","获取公告内容"),

	CMS_UPTODATE_DIRECTIVE("_d_uptodates","contents","获取最新内容"),

	CMS_FRAGMENT_DATA_DIRECTIVE("_d_fragment_data","fragmentData","获取页面片段数据指令");




	@Getter
	private String value;

	@Getter
	private String code;

	@Getter
	private String name;


	DirectiveNameEnum(String value,String code,String name) {
		this.name = name;
		this.value = value;
		this.code = code;
	}
}
