package com.thinkcms.core.config;

import cn.hutool.core.io.FileUtil;
import com.thinkcms.core.constants.Constants;
import com.thinkcms.core.utils.Checker;
import freemarker.template.DefaultObjectWrapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @ClassName: BeanConfig
 * @Author: LG
 * @Date: 2019/5/16 13:29
 * @Version: 1.0
 **/
@Configuration
public class FreeMarkConfig {

    @Bean
    public freemarker.template.Configuration configuration(ThinkCmsConfig thinkCmsConfig) throws IOException {
        freemarker.template.Configuration configuration = new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_28);
        File file = initDirectory(thinkCmsConfig);
        configuration.setDirectoryForTemplateLoading(file);
        configuration.setObjectWrapper(new DefaultObjectWrapper(freemarker.template.Configuration.VERSION_2_3_28));
        configuration.setDefaultEncoding(Constants.DEFAULT_CHARSET_NAME); //这个一定要设置，不然在生成的页面中 会乱码
        return configuration;
    }

    private File initDirectory(ThinkCmsConfig thinkCmsConfig) throws FileNotFoundException {
        File root=null;
        String [] paths=new String[]{thinkCmsConfig.getSourceRootPath(), thinkCmsConfig.getSourceFragmentFilePath(),
        thinkCmsConfig.getSourceTempPath(), thinkCmsConfig.getSiteStaticFileRootPath(),thinkCmsConfig.getLicensePath()
        };
        int i=0;
        for(String path: paths){
            if(Checker.BeNotBlank(path)){
                File file =new File(path);
                if(!file.exists()){
                    file.mkdirs();
                }
                if(2==i){
                    root=file;
                }
            }
            i+=1;
        }
        File file = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX+"license.dat");
        if(file.exists()){
            FileUtil.move(file,new File(thinkCmsConfig.getLicensePath()),true);
        }
        return root;
    }
}
