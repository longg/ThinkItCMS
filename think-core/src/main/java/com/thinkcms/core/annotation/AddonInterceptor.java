/*
 * Copyright (c) 2016-2020 canaanQd. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * canaanQd. You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms of the agreements
 * you entered into with canaanQd.
 *
 */

package com.thinkcms.core.annotation;
import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AddonInterceptor {

}